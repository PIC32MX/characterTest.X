/*! \file  characterTest.c
 *
 *  \brief
 *
 *
 *  \author jjmcd
 *  \date July 26, 2015, 3:01 PM
 *
 * Software License Agreement
 * Copyright (c) 2015 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include <xc.h>
#include <stdint.h>
#include "../include/ConfigBits.h"
#include "../include/TFT.h"
#include "../include/colors.h"

char szString1[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
char szString2[] = "abcdefghijklmnopqrstuvwxyz";

/*! main - */

/*!
 *
 */
int main(void)
{
  char *p;
  int nHorz;

  SetupHardware();
  TFTinit(LANDSCAPE);
  TFTsetBackColorX(NAVY);
  TFTclear();

  TFTsetFont(DJS);
  TFTsetColorX(GOLD);
  TFTputsTT("\r\n\r\nABCDEFGHIJKLMNOPQRSTUVWXYZ\r\n\nabcdefghijklmnopqrstuvwxyz\r\n");
  TFTsetColorX(WHITE);
  TFTprintChar('W',200,30);
  TFTprintChar('W',220,30);
  TFTprintChar('w',240,30);
  TFTprintChar('w',260,30);

  TFTsetFont(SmallFont);
  TFTsetColorX(GOLD);
  TFTputsTT("\r\n\r\nABCDEFGHIJKLMNOPQRSTUVWXYZ\r\n\nabcdefghijklmnopqrstuvwxyz");
  TFTsetColorX(WHITE);
  TFTprintChar('W',200,100);
  TFTprintChar('W',220,100);
  TFTprintChar('w',240,100);
  TFTprintChar('w',260,100);

  TFTsetFont(DJS);
  TFTsetColorX(HOTPINK);
  nHorz=20;
  p=szString1;
  while( *p )
    {
      TFTprintChar(*p,nHorz,140);
      p++;
      nHorz += 10;
    }
  nHorz=20;
  p=szString2;
  while( *p )
    {
      TFTprintChar(*p,nHorz,160);
      p++;
      nHorz += 10;
    }

  while(1)
    {
      
    }

  return 0;
}

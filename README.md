### Test Characters

**characterTest.X** is a very simple PIC32 native application that
displays a few characters on the screen.  It was used to fine tune
some fonts that had minor bugs.
